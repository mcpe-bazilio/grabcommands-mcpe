<?php

namespace pmc;

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\event\Listener;
use pocketmine\permission\DefaultPermissions;
use pocketmine\plugin\PluginBase;
use pocketmine\Server;
use pocketmine\utils\Config;

class GrabCommands extends PluginBase implements Listener {

    /** @var array $cfg Configuration options from config.yml */
    private $cfg;

    /** @var string $commandsTable Имя таблицы с командами MCPE */
    private $commandsTable;

    /** @var string $commandsOwnersTable Имя таблицы с хозяевами команд MCPE */
    private $commandsOwnersTable;

    private $processedCommands = 0;
    private $processedAliases = 0;
    private $processedSubCommands = 0;
    private $updatedRecords = 0;
    private $insertedRecords = 0;
    private $insertedOwners = 0;


    public function onEnable(){
        $this->saveDefaultConfig();
        //Принудительно инициализируем дефолтные разрешения для сервреных команд (иначе они в момент отработки плагина не инициализированы)
        DefaultPermissions::registerCorePermissions();
        $cfg = $this->getCfg();
        $this->commandsTable = $cfg["commandsTable"];
        $this->commandsOwnersTable = $cfg["commandsTable"] . 'Owners';
        $this->collectCommands();
    }

    /**
     * Get configuration
     *
     * @return array
     */
    public function getCfg(){
        if(!isset($this->cfg)){
            $this->cfg = $this->getConfig()->getAll();
        }
        return $this->cfg;
    }

    private function collectCommands(){

        $server = $this->getServer();

        $server->getPluginManager()->registerEvents($this, $this);

        $oBaseLang = $server->getLanguage();
        $Lang = $oBaseLang->getLang();
        $LangFallback = $oBaseLang::FALLBACK_LANGUAGE;
        $usedLangFiles = $Lang . '.ini|' . $LangFallback . '.ini';

        $res = [];

        foreach($server->getCommandMap()->getCommands() as $id => $command){
            $cmd = $command->getName();
            if(!$cmd) continue;
            $commandClass = get_class($command);
            $id = preg_replace('/(?:[^:]+:)([^:]+)/im', '\1', $id);
            if(!isset($res[$cmd])){
                $res[$cmd] = [
                    'cmdID'        => $cmd,
                    'cmd'          => $cmd,
                    'isServer'     => null,
                    'owner'        => null,
                    'version'      => null,
                    'descr'        => $command->getDescription(),
                    'CyrPCdescr'   => 100,
                    'descr_id'     => '',
                    'usage'        => $command->getUsage(),
                    'CyrPCusage'   => 100,
                    'usage_id'     => '',
                    'permissionID' => null,
                    'permission'   => null,
                    'commandClass' => $commandClass,
                    'aliases'      => null,
                    'subcommands'  => null,
                    'permissions'  => [],
                ];
            }

            $rc = &$res[$cmd];

            if($id != $cmd) $rc['aliases'][$id] = null;
            foreach($command->getAliases() as $alias){
                if($alias != $cmd) $rc['aliases'][$alias] = null;
            }

            $perms = explode(';', $command->getPermission());
            foreach($perms as $permissionName){
                $permissionName = trim($permissionName);
                if($permissionName){
                    $PermissionValue = null;
                    /** @var \pocketmine\permission\Permission|null $permission */
                    $permission = $server->getPluginManager()->getPermission($permissionName);
                    if($permission != null){
                        $PermissionValue = $this->NormPerm($permission->getDefault());
                    }
                    $rc['permissions'][$permissionName] = [
                        'description'  => null,
                        'orig'         => null, //TODO Что за хрень? Убрать?
                        'defaultValue' => $PermissionValue,
                        'usage' => null
                    ];

                }
            }
            unset($perms);
            unset($permissionName);
            unset($PermissionValue);
            unset($permission);

            /** @var  $command \pocketmine\command\PluginCommand */
            /** @var  $plugin \pocketmine\plugin\Plugin */
            $plugin = null;
            if(method_exists($command, 'getPlugin')){
                //Точно плагин
                $rc['isServer'] = 0;
                $plugin = $command->getPlugin();
            }else{
                //Похоже на команду сервера, но не факт...
                $rc['isServer'] = 1;

                if(strpos($commandClass, "pocketmine\\command\\defaults\\") === false){
                    //...но на самом деле - не серверная команда,
                    //Пробуем получить объект плагина ориентируясь на совпадение в имени класса команды и плагина
                    //либо поискав свойство 'plugin' в объекте команды
                    $plugin = $this->getAppropriatePlugin($command);
                    if($plugin != null){
                        $rc['isServer'] = 0;
                    }else{
                        //Попробуем получить YML плагина
                        $bcl = $GLOBALS['autoloader'];
                        $path = $bcl->findClass($commandClass);
                        if($path !== null){
                            $path = str_replace($commandClass . '.php', '', $path);
                            $path = str_replace("\\", "/", $path);
                            $path = str_replace("/src", "", $path);
                            $path = str_replace("/", DIRECTORY_SEPARATOR, $path);
                            $path = $path . "plugin.yml";
                            if(file_exists($path)){
                                $pluginYAML = new Config($path, Config::YAML, []);
                                $pluginCFG = $pluginYAML->getAll();
                                $rc['owner'] = $pluginCFG['name'];
                                $rc['version'] = $pluginCFG['version'];
                                $rc['isServer'] = 0;
                                unset($pluginCFG);
                                unset($pluginYAML);
                            }
                        }
                        unset($path);
                        unset($bcl);
                    }
                }
            }

            if($rc['isServer'] == 1){
                $rc['owner'] = $server->getName();
                $rc['version'] = $server->getVersion();

                $msg_id = $command->getDescription();
                if($msg_id){
                    $rc['descr_id'] = $usedLangFiles . $msg_id;
                    $rc['descr'] = $server->getLanguage()->translateString($msg_id);
                }
                $msg_id = $command->getUsage();
                if($msg_id){
                    $rc['usage_id'] = $usedLangFiles . $msg_id;
                    $rc['usage'] = $server->getLanguage()->translateString($msg_id);
                }
                unset($msg_id);
            }elseif($plugin != null){

                $PlaginDescr = $plugin->getDescription(); // объект из plagin.yml
                $rc['owner'] = $PlaginDescr->getName();
                $rc['version'] = $PlaginDescr->getVersion();
                $pluginPermissions = $plugin->getDescription()->getPermissions();
                foreach($pluginPermissions as $pluginPermission){
                    $PermissionName = $pluginPermission->getName();
                    if(isset($rc['permissions'][$PermissionName])){
                        $rc['permissions'][$PermissionName]['description'] = $pluginPermission->getDescription();
                        $rc['permissions'][$PermissionName]['defaultValue'] = $this->NormPerm($pluginPermission->getDefault());
                        $rc['permissions'][$PermissionName]['usage'] = null;
                    }else{
                        $rc['permissions'][$PermissionName] = [
                            'description'  => $pluginPermission->getDescription(),
                            'defaultValue' => $this->NormPerm($pluginPermission->getDefault()),
                            'usage' => null
                            //TODO дописать получение usage из пермишенов
                        ];
                    }

                }
                unset($PlaginDescr);
                unset($pluginPermissions);
                unset($PermissionName);

                // $plugin->getDataFolder(); //Папка плагина
                // $plugin->getResources(); //файлы в папке /resources
                // $plugin->getConfig(); // Настройки из config.yml
            }else{
                $rc['owner'] = 'undefined';
                $rc['version'] = '';
            }
            unset($plugin);
        }
        unset($rc);

        /*
        Анализируем Permissions. Если оно одно, то заполняем поле permissionID и permission.
        Если несколько, то вычленяем из каждого permissionID базу (до ...command(s).) , а остаток трактуем как название подкоманды
        Если среди команд найдется одноименная основной команде, то ее значениями заполняем поле permissionID и permission основнйо команды.
        остальные подкоманды команды оформляем в массив
        Полю 'subcommands' присваиваем полученный массив.
        */
        foreach($res as &$rc){
            if(count($rc['permissions']) == 1){
                foreach($rc['permissions'] as $permissionID => $pm){
                    $rc['permissionID'] = $permissionID;
                    $rc['permission'] = $pm['defaultValue'];
                    if(!$rc['descr'] && $pm['description']){
                        $rc['descr'] = $pm['description'];
                    }
                }
                unset($pm);
                unset($permissionID);
            }elseif(count($rc['permissions']) > 1){
                $subcmd = null;
                //Подготовим имя основной команды для использования в качестве RegExp
                $reCmd = str_replace('#', '\-', str_replace('-', '#', $rc['cmd']));

                foreach($rc['permissions'] as $permissionID => $pm){
                    //если в id пермишена найдется слово command или commands, то считаем то, что после этого слова через точку - Имя под-команды
                    if(!preg_match('/\bcommands?\.([a-z0-9_\-.]+)/im', $permissionID, $matches)){
                        //if(!preg_match('/\b' . $reCmd . '\b\.([a-z0-9_\-.]+)/im', $permissionID, $matches)) //так же считаем, если в id пермишена присутствует имя основной команды.
                        continue;
                    }

                    $subCmdName = $matches[1];
                    //если имя подкоманды начинается с имени основной команды, откусываем его.
                    if(strpos($subCmdName, $rc['cmd'] . '.') === 0 || $subCmdName == $rc['cmd']){
                        $subCmdName = substr($subCmdName, 1 + strlen($rc['cmd']));
                    }

                    if($subCmdName == ''){
                        //случай, когда имя подкоманды совпало с именем команды. Просто расставляем значения атрибутоав основной команды
                        $rc['permissionID'] = $permissionID;
                        $rc['permission'] = $pm['defaultValue'];
                        if(!$rc['descr'] && $pm['description']){
                            $rc['descr'] = $pm['description'];
                        }
                    }else{
                        //Имеем дело с под-командой

                        // Идентификаторы пермишенов, в которых не встречалось имя основной команды - пропускаем
                        if(!preg_match('/(\b' . $reCmd . '\b)/im', $permissionID)) continue;

                        //оставшиеся подкоманды (они из тех id пермишенов, которые содержали имя основной команды)
                        //добавляем их в список подкоманд основной команды
                        $cmdID = $rc['cmd'] . '.' . $subCmdName;
                        $subcmd[$cmdID] = [
                            'cmdID'        => $cmdID,
                            'cmd'          => $subCmdName,
                            'isServer'     => null,
                            'owner'        => null,
                            'version'      => null,
                            'descr'        => $pm['description'],
                            'CyrPCdescr'   => $this->cyrPercent($pm['description'], false),
                            'descr_id'     => null,
                            'usage'        => null,
                            'CyrPCusage'   => 0,
                            'usage_id'     => null,
                            'permissionID' => $permissionID,
                            'permission'   => $pm['defaultValue'],
                            'commandClass' => null,
                        ];
                        unset($cmdID);
                    }
                    unset($subCmdName);
                }
                $rc['subcommands'] = $subcmd;
                unset($subcmd);
                unset($reCmd);
                unset($pm);
                unset($permissionID);
            }
            unset($rc['permissions']);
            //Устанавливаем процент перевода
            $rc['CyrPCdescr'] = $this->cyrPercent($rc['descr'], false);
            $rc['CyrPCusage'] = $this->cyrPercent($rc['usage'], true);
        }

        //file_put_contents('found_commands.json', json_encode($res, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES));
        $this->saveCommandsToDB($res);
    }

    private function NormPerm($perm){
        if($perm == "true") $perm = 'ALL';
        return strtoupper($perm);
    }

    private function getAppropriatePlugin($obj){
        if(is_object($obj)){
            $arClassN = explode("\\", get_class($obj));
        }elseif(is_string($obj)){
            $arClassN = $obj;
        }else{
            return null;
        }
        $plugins = $this->getServer()->getPluginManager()->getPlugins();
        $maxOverlaps = 0;
        $bestAppropriatePlugin = null;
        foreach($plugins as $key => $plugin){
            $arClassP = explode("\\", get_class($plugin));
            for($i = 0; $i < min(count($arClassP), count($arClassN)); $i++){
                if(strtolower($arClassP[$i]) != strtolower($arClassN[$i])){
                    if($maxOverlaps < $i){
                        $maxOverlaps = $i;
                        $bestAppropriatePlugin = $plugin;
                    }
                    break;
                }
            }
        }
        if($bestAppropriatePlugin != null) return $bestAppropriatePlugin;

        if(is_object($obj) && property_exists($obj, 'plugin')){
            $reflection = new \ReflectionClass($obj);
            $property = $reflection->getProperty('plugin');
            $property->setAccessible(true);
            $bestAppropriatePlugin = $property->getValue($obj);
        }
        return $bestAppropriatePlugin;
    }

    private function cyrPercent($str, $bStripTags = false){
        if($bStripTags){
            $str = preg_replace(['/<[^>]+>/', '/\[[^\]]+\]/', '/(?:[a-zа-яё]+\|)+[a-zа-яё]+/i', '%/[a-zа-яё]+%i'], '', $str);
        }
        $onlyW = preg_replace('/[^а-яёa-z]/imu', '', $str);
        $lenW = strlen($onlyW);
        if($lenW == 0) return 100;
        $onlyCyr = preg_replace('/[^а-яё]/imu', '', $onlyW);
        return intval(strlen($onlyCyr) * 100 / $lenW);
    }

    private function saveCommandsToDB($res){
        $fnId = __FUNCTION__;
        $cfg = $this->getCfg();
        $host = $cfg["host"];
        $port = $cfg["port"];
        $username = $cfg["username"];
        $password = $cfg["password"];
        $database = $cfg["database"];
        $commandsTable = $this->commandsTable;
        $commandsOwnersTable = $this->commandsOwnersTable;

        $db = @new \mysqli($host, $username, $password, null, $port);
        if($db->connect_error){
            return $this->ERROR("НЕ удалось инициализировать соединение с базой данных", $fnId);
        }
        if(!$db->select_db($database)){
            @$db->close();
            return $this->ERROR("НЕ удалось выбрать БД $database", $fnId);
        }

        if(\mysqli_num_rows($db->query("SHOW TABLES LIKE '{$commandsOwnersTable}'")) == 0){

            $query = "CREATE TABLE `$commandsOwnersTable` (
  `ownerID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID хозяина команды',
  `owner` varchar(150) NOT NULL COMMENT 'Название сервера или плагина',
  `version` varchar(45) NOT NULL DEFAULT '' COMMENT 'Версия сервера или плагина',
  `isServer` tinyint(1) DEFAULT NULL COMMENT 'Флаг - \"сервер\" (иначе - плагин)\n',
  `descr` varchar(2048) DEFAULT NULL COMMENT 'Описание сервера или плагина',
  `CyrPCdescr` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Процент символов кириллицы в строке Descr',
  `URLPhar` varchar(45) DEFAULT NULL COMMENT 'Ссылка на скачивание Phar архива сервера или плагина',
  `URLcvs` varchar(45) DEFAULT NULL COMMENT 'Ссылка на CVS, где располагаются исходники сервера или плагина.',
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `show` tinyint(1) DEFAULT '1' COMMENT 'Флаг \"Отображать на сайте\"',
  PRIMARY KEY (`ownerID`),
  UNIQUE KEY `IX_NameVersion` (`owner`,`version`),
  KEY `IX_NameServer` (`isServer`,`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='Хозяева команд MCPE - сервера и плагины различных версий. (видимость, где скачать описания и.т.п.)';
";
            If(!$db->query($query)){
                $this->ERROR("Ошибка при попытке создать таблицу $commandsTable. " . $db->error);
            }
        }

        if(\mysqli_num_rows($db->query("SHOW TABLES LIKE '{$commandsTable}'")) == 0){

            $query = "CREATE TABLE `$commandsTable` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный ID',
  `aliasOf` int(11) DEFAULT NULL COMMENT 'ID команды, для которой эта является дочерней',
  `subCmdOf` int(11) DEFAULT NULL COMMENT 'ID команды, для которой эта является дочерней',
  `cmdID` varchar(250) NOT NULL COMMENT 'команда.подкоманда',
  `cmd` varchar(100) NOT NULL COMMENT 'команда',
  `isServer` tinyint(1) DEFAULT NULL COMMENT 'Флаг - \"серверная команда\"\n',
  `ownerID` int(11) DEFAULT NULL COMMENT 'ID хозяина команды',
  `owner` varchar(150) DEFAULT NULL COMMENT 'Название сервера или плагина',
  `version` varchar(45) DEFAULT NULL COMMENT 'Версия сервера или плагина',
  `descr` varchar(1024) DEFAULT NULL COMMENT 'Описание команды',
  `CyrPCdescr` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Процент символов кириллицы в строке Descr',
  `descr_id` varchar(1024) DEFAULT NULL COMMENT 'Источник текста для descr',
  `usage` varchar(1024) DEFAULT NULL COMMENT 'Правило использования команды',
  `CyrPCusage` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Процент символов кириллицы в строке Usage',
  `usage_id` varchar(1024) DEFAULT NULL COMMENT 'Источник текста для usage',
  `permission` varchar(50) DEFAULT NULL COMMENT 'Права доступа: op, true ....',
  `permissionID` varchar(255) DEFAULT NULL COMMENT 'ID разрешения',
  `commandClass` varchar(255) DEFAULT NULL COMMENT 'Имя класса команды\n',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descr_final` varchar(1024) DEFAULT NULL COMMENT 'Финальное описание команды. При обновлении записи не трогается. Используется для вывода в HTML',
  `usage_final` varchar(1024) DEFAULT NULL COMMENT 'Финальное описание использования. При обновлении записи не трогается. Используется для вывода в HTML',
  PRIMARY KEY (`id`),
  KEY `IX_cmd` (`cmd`,`owner`,`version`),
  KEY `IX_cmdID` (`owner`,`version`,`cmdID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Комманды MCPE';";
            If(!$db->query($query)){
                $this->ERROR("Ошибка при попытке создать таблицу $commandsTable. " . $db->error);
            }
        }

        $this->processedCommands = 0;
        $this->processedAliases = 0;
        $this->processedSubCommands = 0;
        $this->updatedRecords = 0;
        $this->insertedRecords = 0;
        $this->insertedOwners = 0;

        foreach($res as $arCmd){
            $this->processedCommands++;
            $newID = $this->mcpe_cmd_UPSERT($db, $arCmd);
            if($newID == null){
                continue;
            }
            if(is_array($arCmd['aliases']) && count($arCmd['aliases']) > 0){
                foreach($arCmd['aliases'] as $alias => $vl){
                    $this->processedAliases++;
                    $this->mcpe_cmd_aliases_UPSERT($db, $alias, $newID);
                }
            }
            if(is_array($arCmd['subcommands']) && count($arCmd['subcommands']) > 0){
                foreach($arCmd['subcommands'] as $arSubCommand){
                    $this->processedSubCommands++;
                    $this->mcpe_subcmd_UPSERT($db, $arSubCommand, $newID);
                }
            }
        }
        $total = $this->processedCommands + $this->processedAliases + $this->processedSubCommands;
        $this->LOG("Найдено команд: {$this->processedCommands}, Алиасов: {$this->processedAliases}, Подкоманд: {$this->processedSubCommands}");
        $this->LOG("Всего записей: $total. Вставлено: {$this->insertedRecords}, обновлено: {$this->updatedRecords} в таблице §l$database.$commandsTable");
        $this->LOG("Новых плагинов/сервреров добавлено: {$this->insertedOwners} в таблице §l$database.$commandsOwnersTable");

        return true;
    }

    private function ERROR(string $msg, string $fnId = null){
        $msg = $this->getCfg()["prefix"] . ($fnId == null ? '' : '§c[' . $fnId . '] §b') . $msg;
        Server::getInstance()->getLogger()->error(trim($msg));
        return null;
    }

    /**
     * @param \mysqli $db
     * @param array   $arCmd
     *
     * @return int|null
     */
    private function mcpe_cmd_UPSERT($db, $arCmd){
        $fnId = __FUNCTION__;

        $cmdID = $arCmd['cmdID'];
        $cmd = $arCmd['cmd'];
        $owner = $arCmd['owner'];
        $version = $arCmd['version'];
        $descr = $arCmd['descr'];
        $usage = $arCmd['usage'];
        $descr_id = $arCmd['descr_id'];
        $usage_id = $arCmd['usage_id'];
        $CyrPCdescr = $arCmd['CyrPCdescr'];
        $CyrPCusage = $arCmd['CyrPCusage'];
        $permissionID = $arCmd['permissionID'];
        $permission = $arCmd['permission'];
        $isServer = $arCmd['isServer'];
        $commandClass = $arCmd['commandClass'];
        $aliasOf = null;

        $ownerID = $this->getOwnerID($db, $owner, $version, $isServer);
        if(!$ownerID){
            return $this->ERROR("Не удалось получить ownerID для $owner v $version", $fnId);
        }

        $commandsTable = $this->commandsTable;;

        $sql = "SELECT id FROM $commandsTable WHERE cmdID=? AND ownerID=?";
        $stmt = $db->prepare($sql);
        if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
        $stmt->bind_param("si", $cmdID, $ownerID);
        if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
        $stmt->bind_result($id);
        if($stmt->fetch()){
            $stmt->close();
            $sql = "UPDATE $commandsTable SET isServer=?, owner=?, version=?, 
                      descr=?, CyrPCdescr=?, descr_id=?, 
                      `usage`=?, CyrPCusage=?, usage_id=?, 
                      permission=?, permissionID=?, commandClass=? WHERE id=?";
            $stmt = $db->prepare($sql);
            if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
            $stmt->bind_param("isssississssi", $isServer, $owner, $version, $descr, $CyrPCdescr, $descr_id, $usage, $CyrPCusage, $usage_id, $permission, $permissionID, $commandClass, $id);
            if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
            $stmt->close();
            $this->updatedRecords++;
        }else{
            $sql = "INSERT INTO $commandsTable (cmdID, cmd, isServer, ownerID, owner, version, 
                    descr, CyrPCdescr, descr_id, `usage`, CyrPCusage, usage_id, 
                    permission, permissionID, commandClass, descr_final, usage_final) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
            $stmt->bind_param("ssiisssississssss",
                $cmdID, $cmd, $isServer, $ownerID, $owner, $version,
                $descr, $CyrPCdescr, $descr_id, $usage, $CyrPCusage, $usage_id,
                $permission, $permissionID, $commandClass, $descr, $usage);
            if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
            $id = $db->insert_id;
            $stmt->close();
            $this->insertedRecords++;
        }
        return $id;
    }

    /**
     * UPSERT в таблицу хозяев серверов/плагинов
     *
     * @param \mysqli $db
     * @param string  $owner
     * @param string  $version
     *
     * @return int|null
     */
    private function getOwnerID($db, $owner, $version, $isServer){
        $fnId = __FUNCTION__;
        $commandsOwnersTable = $this->commandsOwnersTable;

        $sql = "SELECT ownerID FROM $commandsOwnersTable WHERE owner=? AND version<=>?";
        $stmt = $db->prepare($sql);
        if($stmt === false){
            return $this->ERROR("DB error: " . $db->error, $fnId);
        }
        $stmt->bind_param("ss", $owner, $version);
        if(!$stmt->execute()){
            $stmt->close();
            return $this->ERROR("DB error: " . $stmt->error, $fnId);
        }
        $stmt->bind_result($ownerID);
        if(!$stmt->fetch()){
            $sql = "INSERT INTO $commandsOwnersTable (owner, version, isServer) VALUES (?,?,?)";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return $this->ERROR("DB error: " . $db->error, $fnId);
            }
            $stmt->bind_param("ssi", $owner, $version, $isServer);
            if(!$stmt->execute()){
                $stmt->close();
                return $this->ERROR("DB error: " . $db->error, $fnId);
            }
            $ownerID = $db->insert_id;
            $stmt->close();
            $this->insertedOwners++;
        }
        return $ownerID;
    }

    private function dbErr($db, $stmt, $fnId){
        $err = '';
        if($db && $db->error){
            $err .= "DB error: " . $db->error . ' ';
        }
        if($stmt){
            if($stmt->error){
                $err .= "STMT error: " . $stmt->error . ' ';
            }
            $stmt->close();
        }
        return $this->ERROR($err, $fnId);
    }

    /**
     * @param \mysqli $db
     * @param string  $alias
     * @param int     $aliasOf
     * @param int     $insertedRecords
     * @param int     $updatedRecords
     *     *
     *
     * @return int|null
     */
    private function mcpe_cmd_aliases_UPSERT($db, $alias, $aliasOf){
        $fnId = __FUNCTION__;
        $commandsTable = $this->commandsTable;

        $sql = "SELECT id FROM $commandsTable WHERE cmd=? AND aliasOf<=>?";
        $stmt = $db->prepare($sql);
        if($stmt === false){
            return $this->ERROR("DB error: " . $db->error, $fnId);
        }
        $stmt->bind_param("si", $alias, $aliasOf);
        if(!$stmt->execute()){
            $stmt->close();
            return $this->ERROR("DB error: " . $stmt->error, $fnId);
        }
        $stmt->bind_result($id);
        if($stmt->fetch()){
            $stmt->close();
            $sql = "UPDATE $commandsTable SET cmdID=? WHERE id=?";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return $this->ERROR("DB error: " . $db->error, $fnId);
            }
            $stmt->bind_param("si", $alias, $id);
            if(!$stmt->execute()){
                $stmt->close();
                return $this->ERROR("DB error: " . $stmt->error, $fnId);
            }
            $stmt->close();
            $this->updatedRecords++;
        }else{

            $sql = "INSERT INTO $commandsTable (aliasOf, cmdID, cmd) VALUES (?,?,?)";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return $this->ERROR("DB error: " . $db->error, $fnId);
            }
            $stmt->bind_param("iss", $aliasOf, $alias, $alias);
            if(!$stmt->execute()){
                $stmt->close();
                return $this->ERROR("DB error: " . $db->error, $fnId);
            }
            $id = $db->insert_id;
            $stmt->close();
            $this->insertedRecords++;
        }
        return $id;
    }

    /**
     * @param \mysqli $db
     * @param array   $arSubCommand
     *
     * @return int|null
     */
    private function mcpe_subcmd_UPSERT($db, $arSubCommand, $subCmdOf){
        $fnId = __FUNCTION__;

        $cmdID = $arSubCommand['cmdID'];
        $cmd = $arSubCommand['cmd'];
        $descr = $arSubCommand['descr'];
        $descr_id = $arSubCommand['descr_id'];
        $CyrPCdescr = $arSubCommand['CyrPCdescr'];
        $permissionID = $arSubCommand['permissionID'];
        $permission = $arSubCommand['permission'];

        $commandsTable = $this->commandsTable;;

        $sql = "SELECT id FROM $commandsTable WHERE cmdID=? AND subCmdOf<=>?";
        $stmt = $db->prepare($sql);
        if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
        $stmt->bind_param("si", $cmdID, $subCmdOf);
        if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
        $stmt->bind_result($id);
        if($stmt->fetch()){
            $stmt->close();
            $sql = "UPDATE $commandsTable SET descr=?, CyrPCdescr=?, descr_id=?, permission=?, permissionID=? WHERE id=?";
            $stmt = $db->prepare($sql);
            if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
            $stmt->bind_param("sisssi", $descr, $CyrPCdescr, $descr_id, $permission, $permissionID, $id);
            if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
            $stmt->close();
            $this->updatedRecords++;
        }else{

            $sql = "INSERT INTO $commandsTable (subCmdOf, cmdID, cmd, descr, CyrPCdescr, descr_id, permission, permissionID, descr_final) VALUES (?,?,?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            if($stmt === false) return $this->dbErr($db, $stmt, $fnId);
            $stmt->bind_param("isssissss",
                $subCmdOf, $cmdID, $cmd,
                $descr, $CyrPCdescr, $descr_id,
                $permission, $permissionID, $descr);
            if(!$stmt->execute()) return $this->dbErr($db, $stmt, $fnId);
            $id = $db->insert_id;
            $stmt->close();
            $this->insertedRecords++;
        }
        return $id;
    }

    private function LOG(string $msg, string $fnId = null){
        $msg = $this->getCfg()["prefix"] . ($fnId == null ? '' : '§c[' . $fnId . '] §b') . $msg;
        Server::getInstance()->getLogger()->info(trim($msg));
        return null;
    }

    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args){
        switch($cmd->getName()){
            case "grabcmd":
                $this->collectCommands();;
                break;

        }
    }

}
